﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var items = new List<Item>
            {
                new Item { ParentId = string.Empty, ReferentId = "cf86ee28fbf84e53934d099ff6cee19f" },
                new Item { ParentId = "cf86ee28fbf84e53934d099ff6cee19f" , ReferentId = "a65f396c88d34a6c8b5034363adc28a6" },
                new Item { ParentId = "cf86ee28fbf84e53934d099ff6cee19f|a65f396c88d34a6c8b5034363adc28a6" , ReferentId = "1639739816b94c47bf71ce2d39966f58" },
                new Item { ParentId = "cf86ee28fbf84e53934d099ff6cee19f|a65f396c88d34a6c8b5034363adc28a6|1639739816b94c47bf71ce2d39966f58" , ReferentId = "0145bd76106a4a32912ae7b40557dd1f" },
                new Item { ParentId = "cf86ee28fbf84e53934d099ff6cee19f|a65f396c88d34a6c8b5034363adc28a6|1639739816b94c47bf71ce2d39966f58" , ReferentId = "0145bd76106a4a32912ae7b40557dd11" },
                new Item { ParentId = "cf86ee28fbf84e53934d099ff6cee19f|a65f396c88d34a6c8b5034363adc28a6|1639739816b94c47bf71ce2d39966f58|0145bd76106a4a32912ae7b40557dd1f" , ReferentId = "0145bd76106a4a32912ae7b40557dd19" },
            };

            foreach (var item in items.Where(m => string.IsNullOrWhiteSpace(m.ParentId)))
            {
                var lv1s = items.Where(m => m.ParentId == item.ReferentId).ToList();
                AddChilds(lv1s, items);
            }

            Console.WriteLine();
        }

        static void AddChilds(List<Item> i, List<Item> items)
        {
            Action<List<Item>> func = null;
            func = n => n.ForEach(o =>
            {
                o.Child.AddRange(items.Where(m => m.ParentId == $"{o.ParentId}|{o.ReferentId}"));
            });
            func(i);
            var childs = i.SelectMany(m => m.Child).ToList();
            if (childs.Any())
            {
                AddChilds(childs, items);
            }
        }
    }

    public class Item
    {
        public string ParentId { get; set; }
        public string ReferentId { get; set; }
        public int Type { get; set; }
        public List<Item> Child { get; set; }
        public Item()
        {
            this.Child = new List<Item>();
        }
    }
}
